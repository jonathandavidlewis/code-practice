
import ast
import sys
import os

from termcolor import colored, cprint

prompts = [
    ("set name to six", "name = 6"),
    ("set name to string hello", 'name = "hello"'),
    ("set count to seven", "count = 7"),
    ("seven is less than twelve", "7 < 12"),
    ("cat is equal to dog", "cat == dog"),
    ("string cat is equal to string dog", "'cat' == 'dog'"),
    ("if max is less than current:\n  return current", "if max < current:\n  return current")
]


def read_multiline(prompt):
    message = "* press ctrl + d to send."
    text = colored(message, 'blue', attrs=['reverse', 'blink'])
    print("------------------")
    print(prompt)
    print("------------------")
    print(text)
    return sys.stdin.read()

def get_input(prompt: str):
    if '\n' in prompt:
        return read_multiline(prompt)
    else:
        return input(prompt + "\n")

press_enter_message = colored("Press enter to continue...", 'cyan')

def clear_terminal():
    os.system('cls' if os.name == 'nt' else 'clear')

def compare_answer(response, answer):
    correct = False
    try:
        user_tree = ast.parse(response)
        expected_tree = ast.parse(answer)
        correct = ast.dump(user_tree) == ast.dump(expected_tree)
    except:
        pass
    finally:
        return correct
        

def play_intro():
    clear_terminal()
    welcome_message = colored("Welcome to the practice Python game!", 'green', attrs=['reverse', 'blink'])
    print(welcome_message)
    print()
    print("I will give you a statement in English.")
    print("Then you must type the Python equivalent of the statement.")
    print()
    input(press_enter_message)
    clear_terminal()
    print("If you get it right, I'll give you 10 points.")
    print()
    input(press_enter_message)
    print()
    print("If you get it wrong, I'll show you the answer and you can try again.")
    print()
    input(press_enter_message)
    clear_terminal()
    print(colored("Here's an example:", "cyan"))
    print()
    print("string cat is equal to string dog")
    input()
    print(colored("the answer is:", "cyan"))
    print()
    print("'cat' == 'dog'")
    input()
    print(colored("You just write the statement in Python!", "cyan"))
    input()
    clear_terminal()
    print(colored("One more note:", "blue", attrs=['reverse']))
    print()
    print("Sometimes, you need to write multiple lines.")
    print()
    eof = colored("ctrl + d", "green")
    input(f"When you do, just use {eof} to submit your answer.")
    print()
    print("Ready... Set... Go!")
    print()
    input(press_enter_message)
    clear_terminal()

play_intro()


def play_game():
    total_points = 0
    for prompt, answer in prompts:
        correct = False
        possible_points = 10
        while not correct:
            response = get_input(prompt)
            correct = compare_answer(response, answer)
            if correct:
                print()
                print("Correct! 🙌" )
                print()
                total_points += possible_points
                if possible_points > 0:
                    print(f"you earned {possible_points} points.")
                    print()
                input(press_enter_message)
                clear_terminal()
            else:
                possible_points = 0
                print()
                print("Incorrect")
                print(answer)
    print("CONGRATULATIONS!")
    print(colored(f"You earned {total_points} points!", "magenta", attrs=["reverse"]))
    print(colored(f"You just practiced converting English to Python and thinking with logical instructions!", "cyan"))
    

play_game()
